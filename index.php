<?php

use \Util\ApiCaller;
use \Util\UserPostsDataProcessor as DataProcessor;

require __DIR__ . '/Util/ApiCaller.php';
require __DIR__ . '/Util/UserPostsDataProcessor.php';

echo 'script started </br>';
$caller = new \Util\ApiCaller();
echo 'caller established </br>';

$idData = $caller->makeACall(
	ApiCaller::POST,
	'https://api.supermetrics.com/assignment/register',
	[
		'client_id' => 'ju16a6m81mhid5ue1z3v2g0uh',
		'email' => 'dejf.herman@gmail.com',
		'name' => 'DavidHerman'
	]
);

echo 'token call finished </br>';

$idData = json_decode($idData, true);
$token = $idData['data']['sl_token'];

$postsArray = [];
for($page = 1; $page <= 10; $page++) {
	$dataBatch = $caller->makeACall(
		ApiCaller::GET,
		'https://api.supermetrics.com/assignment/posts?sl_token=' . $token . '&page=' . $page
	);

	$dataBatch = json_decode($dataBatch, true)['data']['posts'];
	$postsArray = array_merge($postsArray, $dataBatch);
}

echo 'data call finished </br>';

echo '</br> Posts: ' . count($postsArray) . '</br> ';

$processer = new DataProcessor($postsArray);
echo '</br>';
$processer->printMonthlyInfo();
$processer->printWeeklyInfo();
