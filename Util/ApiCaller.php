<?php

namespace Util;

/**
 * Class ApiCaller
 * @package Util
 *
 *          Makes all the important calls. Tries not to let that get into it's head.
 *
 */
class ApiCaller
{
	/** RESTful methods */
	const GET = 'GET';
	const POST = 'POST';
	const PUT = 'PUT';
	const DELETE = 'DELETE';
	const PATCH = 'PATCH';


	/**
	 * @param string		$method
	 * @param string		$url
	 * @param array|null	$data
	 * @return string
	 */
	public function makeACall(string $method, string $url, array $data = null) : string
	{
		$handle = curl_init();

		$options = [
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
		];

		switch ($method) {
			case self::GET:
				// nothing to do here, really
				break;
			case self::POST:
				$options += [
					CURLOPT_POST => true,
					CURLOPT_POSTFIELDS => $data,
				];
				break;
			case self::PUT:
				// implement specifics here
				break;
			case self::DELETE:
				// implement specifics here
				break;
			case self::PATCH:
				// implement specifics here
				break;
			default:
				throw new \Exception('Unknown REST method.');
		}

		curl_setopt_array($handle, $options);
		curl_setopt($handle, CURLOPT_VERBOSE, true);
		$result = curl_exec($handle);
		curl_close($handle);
		return $result;
	}
}
