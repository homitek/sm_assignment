<?php

namespace Util;

/**
 * Class UserPostsDataProcessor
 * @package Util
 *
 *          Holds all post data, calculates statistics over them and takes care
 *          of display functions too. Knows how to do averages.
 *          Is a little overloaded, but doesn't complain.
 *
 */
class UserPostsDataProcessor
{
	/**
	 * There's more than one kind of average
	 */
	const MEAN = 'MEAN';
	const MEDIAN = 'MEDIAN';
	const MODE = 'MODE';

	/**
	 * @var array
	 * Multilevel array structured as follows
	 * 	[year] => {
	 * 		[month] => {
	 *			[0] => {post data}
	 * 			...
	 * 		}
	 * 	}
	 */
	private $postsByMonth;
	/**
	 * @var array
	 * Multilevel array structured as follows
	 * 	[year] => {
	 * 		[week] => {
	 *			[0] => {post data}
	 * 			...
	 * 		}
	 * 	}
	 */
	private $postsByWeek;


	/**
	 * UserPostsDataProcessor constructor.
	 * Saves supplied posts into inner variable arrays structured by time (months/weeks).
	 *
	 * @param array $posts
	 */
	public function __construct(array $posts)
	{
		// in a larger task, posts should get their own DTO
		// here the initial JSON-decoded array will suffice
		foreach ($posts as $key => $post) {
			$dTime = new \DateTime($post['created_time']);
			$this->postsByMonth[$dTime->format('Y')][$dTime->format('m')][] = $post;
			$this->postsByWeek[$dTime->format('Y')][$dTime->format('W')][] = $post;
		}
	}


	/**
	 * Prints required results by month.
	 * In a larger task would be in a separate display-related class
	 * or templating system.
	 */
	public function printMonthlyInfo() : void
	{
		echo '<h3>Monthly stats:</h3>';
		foreach ($this->postsByMonth as $year => $months) {
			foreach ($months as $month => $posts) {
				echo '<h4>Year ' . $year . ', month ' . $month . ':</h4>';
				echo 'Average post length is '
					. $this->getAvgPostLength($posts) . '.</br>';
				$theLongest = $this->getLongestPost($posts);
				echo 'Longest post is ' . $theLongest['id'] . ', with '
					. strlen($theLongest['message']) . ' characters.</br>';
				echo 'Average posts by a single user are ' .
					$this->getAvgUserPosts($posts) . '.</br>';
			}
		}
	}


	/**
	 * Prints required results by week.
	 * In a larger task would be in a separate display-related class
	 * or templating system.
	 */
	public function printWeeklyInfo() : void
	{
		echo '<h3>Weekly stats:</h3>';
		foreach ($this->postsByWeek as $year => $weeks) {
			foreach ($weeks as $week => $posts) {
				echo '<h4>Year ' . $year . ', week ' . $week . ':</h4>';
				echo 'Total post count is ' . count($posts) . '.</br>';
			}
		}
	}


	/**
	 * Returns the average post length from the supplied array of posts.
	 *
	 * @param array $posts
	 * @return float
	 */
	private function getAvgPostLength(array $posts) : float
	{
		$postLengths = [];
		foreach ($posts as $post) {
			$postLengths[] = strlen($post['message']);
		}
		return $this->calculateAverage($postLengths);
	}


	/**
	 * Finds the longest post from the supplied array and returns it.
	 * Returns null when array is empty or posts have 0-length messages.
	 *
	 * @param array $posts
	 * @return null|array
	 */
	private function getLongestPost(array $posts) : ?array
	{
		$maxLen = 0;
		$longestPost = null;
		foreach ($posts as $post) {
			if (strlen($post['message']) > $maxLen) {
				$maxLen = strlen($post['message']);
				$longestPost = $post;
			}
		}
		return $longestPost;
	}


	/**
	 * Returns the average number of posts by a single user from the supplied
	 * array of posts.
	 *
	 * @param array $posts
	 * @return float
	 */
	private function getAvgUserPosts(array $posts) : float
	{
		$userPostCounts = [];
		foreach ($posts as $post) {
			$userPostCounts[$post['from_id']]++;
		}
		return $this->calculateAverage($userPostCounts);
	}


	/**
	 * Calculates the average from an array of numeric values.
	 * Has different modes of averaging. Returns null when supplied with
	 * an unknown method.
	 *
	 * @param array  $values	(array of int/float/double)
	 * @param string $avgMethod
	 *
	 * @return float|null
	 */
	private function calculateAverage(array $values, string $avgMethod = self::MEAN) : ?float
	{
		$avg = null;
		switch ($avgMethod) {
			case self::MEAN:
				$avg = array_sum($values) / count($values);
				break;
			case self::MEDIAN:
				sort($values);
				$avg =
					( $values[(int) ceil(count($values) / 2) - 1]
						+ $values[(int) floor(count($values) / 2) - 1] )
					/ 2;
				break;
			case self::MODE:
				// probably not needed at the moment, implement if asked for
				break;
			default:
				return $avg;
		}
		return (float) $avg;
	}
}
